FROM nginx:stable-alpine

COPY ./www /usr/share/nginx/html

COPY ./landings.conf /etc/nginx/conf.d/landings.conf

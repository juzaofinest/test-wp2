/* eslint-disable import/no-extraneous-dependencies */
import OpenBrowserPlugin from 'open-browser-webpack-plugin';
import FaviconsWebpackPlugin from 'favicons-webpack-plugin';
import ExtractTextPlugin from 'extract-text-webpack-plugin';
import MinifyPlugin from 'babel-minify-webpack-plugin';
import WebpackShellPlugin from 'webpack-shell-plugin';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import DotenvPlugin from 'webpack-dotenv-plugin';
import ZopfliPlugin from 'zopfli-webpack-plugin';
import webpack from 'webpack';
import dotenv from 'dotenv';
import yargs from 'yargs';
import path from 'path';
import fs from 'fs';

import { rules } from './loaders.config';

import siteconfig from './siteconfig.json';

const NODE_ENV = process.env.NODE_ENV === 'production' ? 'production' :
  'development';
const dev = NODE_ENV === 'development';
const prod = NODE_ENV === 'production';

dotenv.config({
  path: [
    dev && path.join(__dirname, '.env.dev'),
    path.join(__dirname, '.env'),
  ].filter(Boolean).filter(fs.existsSync).shift(),
});

const dotenvPlugin = new DotenvPlugin({
  sample: './_env',
  path: [
    path.join(__dirname, '.env'),
    dev && path.join(__dirname, '.env.dev'),
  ].filter(Boolean),
  allowEmptyValues: true,
});


const PORT = yargs.argv.port || 3000;
const relativeBuildPath = './dist/www';


export default function (env = {}) {
  const landing = env.landing || Object.keys(siteconfig).pop();
  const config = siteconfig[landing];
  if (typeof config === 'undefined') throw new Error('>>> No config found in siteconfig');

  console.log('env> ', env);

  const templateFile = [
    'index.html',
    'index.pug',
  ].map(file => path.join(__dirname, ...config.src.replace('./', '').split('/'), file))
    .filter(fs.existsSync).pop();
  const entryFile = [
    'index.js',
    'index.jsx',
  ].map(file => path.join(__dirname, ...config.src.replace('./', '').split('/'), file))
    .filter(fs.existsSync).pop();
  return {
    entry: {
      frontend: entryFile,
    },
    output: {
      path: path.join(__dirname, relativeBuildPath),
      publicPath: '/',
      filename: 'js/[name].js?[hash]',
    },

    devServer: {
      port: PORT,
      host: '0.0.0.0',
      disableHostCheck: true,
    },

    resolve: {
      extensions: [ '*', '.js', '.jsx', '.json' ],
      alias: {
        _api: path.resolve(__dirname, 'src', 'api'),
      },
    },
    module: {
      rules,
    },

    devtool: NODE_ENV === 'production' ? false : 'inline-source-map',
    plugins: [
      new HtmlWebpackPlugin({
        title: 'CyberPunk',
        template: templateFile,
        filename: 'index.html',
      }),
      new ExtractTextPlugin({
        filename: 'css/[name].css?[hash]',
        allChunks: true,
      }),
      new webpack.LoaderOptionsPlugin({
        minimize: prod,
        options: {
          stylus: {
            import: [
              '~stylus-mixins/index.styl',
              path.resolve(__dirname, './src/common.styl'),
            ],
          },
        },
      }),
      new webpack.DefinePlugin({
        'process.env.OPTIMIZE_ID': JSON.stringify(config.optimize_id),
      }),
      new FaviconsWebpackPlugin({
        // Your source logo
        logo: './src/favicon32.png',
        prefix: 'assets/icons-[hash]/',
      }),

      dotenvPlugin,

      dev && ![ 80, 443 ].includes(PORT) && new OpenBrowserPlugin({ url:`http://local.rain.wtf:${PORT}` }),

      prod && new MinifyPlugin({
        removeConsole: true,
      }, {
        comments: false,
      }),

      prod && new ZopfliPlugin({
        asset: '[path].gz',
        algorithm: 'zopfli',
        // test: /\.(js|html)$/,
        threshold: 10240,
        minRatio: 0.8,
      }),

      prod && new WebpackShellPlugin({
        onBuildStart: [`rimraf ${relativeBuildPath}`],
        onBuildEnd: [
          'cp -f ./Dockerfile ./dist/Dockerfile',
          'cp -f ./landings.conf ./dist/landings.conf',
        ],
      }),
    ].filter(Boolean),
  };
}

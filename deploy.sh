yarn build --env.landing=sportx2
docker build -t docker.rain.wtf/yourbet-landings-sportx2:latest -f ./dist/Dockerfile ./dist
docker push docker.rain.wtf/yourbet-landings-sportx2:latest

ssh -t frappe '
  docker pull docker.rain.wtf/yourbet-landings-sportx2:latest
  docker stop yourbet-landings-sportx2
  docker rm yourbet-landings-sportx2
  docker run -d \
    --net nginx-proxy \
    --name=yourbet-landings-sportx2 \
    --restart=unless-stopped \
    -e VIRTUAL_HOST=sportx2.yourbet.com \
    -e LETSENCRYPT_HOST=sportx2.yourbet.com \
    -e LETSENCRYPT_EMAIL="a.lavrentev@rain.wtf" \
    docker.rain.wtf/yourbet-landings-sportx2:latest
'

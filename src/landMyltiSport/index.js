import Handlebars from 'handlebars/dist/handlebars';
// import 'picturefill';
import _ from 'lodash';
import queryString from 'query-string';

import 'reset.css';
import 'normalize.css';
import './checkbox.css';
import './style.styl';
import '../Fonts/Roboto/stylesheet.css';

import { getTranslations, register } from '../api';


const transUrl = `${process.env.LANDINGS_API_URL || ''}/translations/myltisport${window.location.search}`;

const getRegisterHref = () => {
  const redirectButton = document.getElementById('redirect-href');
  // const lang = [ 'ru', 'en', 'tr', 'uk', 'ka' ].includes(translate('language')) ? translate('language') : 'en';
  const lang = 'en';
  const params = [ 'cid', 'postback', 'utm_term' ];
  const searchPrams = queryString.parse(window.location.search);
  let paramsString = params.map((p) => {
    const val = searchPrams[p];
    return val ? `${p}=${val}` : '';
  }).filter(Boolean).join('&');
  if (paramsString.length) {
    paramsString = `?${paramsString}`;
  }
  const returnUrl = `https://yourbet.com/${lang}/register${paramsString}`;
  redirectButton.href = returnUrl;
  return returnUrl;
};
fetch(transUrl, {
  credentials: 'include',
})
  .then(response => response.json())
  .catch(() => {})
  .then((translations) => {
    const data = {
      header: _.get(translations, 'Header.descr', 'Bet on the best!'),
      headerDescr: _.get(translations, 'Header.descr2', 'Be the best!'),
      modalHeader: _.get(translations, 'ModalFields.modalHeader', 'Bonus on start'),
      modalLink: _.get(translations, 'ModalFields.modalLink', 'Already have an account?'),
      modalEnterLink: _.get(translations, 'ModalFields.modalEnterLink', 'Log in'),
      placeholder: _.get(translations, 'ModalFields.emailPlaceholder', 'Your email'),
      modalButton: _.get(translations, 'ModalFields.modalButton', 'Get bonus'),
      modalWindowText: _.get(translations, 'PopupField.popupField', 'Please click on the activation link sent to you by email'),
      termsIagree: _.get(translations, 'TandC.iAgreeWith', 'I agree with'),
      allrightBtn: _.get(translations, 'PopupField.popupProof', 'OK'),
      termsHref: _.get(translations, 'TandC.termsAndConditions', 'terms and conditions'),
      popupHeader: _.get(translations, 'PopupField.popupHeader', 'Check your email'),
      termsAnd: _.get(translations, 'TandC.and', 'and'),
      termsPrivacyPolicy: _.get(translations, 'TandC.privacyPolicy', 'privacy policy'),
      termsCitizen: _.get(translations, 'TandC.notACitizen', 'I am not a citizen of United States of America, Spain, Italy, France, United Kingdom, Russian Federation, Israel, Latvia'),
      getStartedTitle: _.get(translations, 'MainContentSteps.mainContentStepsDescr', 'Get started in 3 easy steps'),
      mainContentDescrOne: _.get(translations, 'MainContentSteps.mainContentStepOne', 'Open an account'),
      mainContentDescrTwo: _.get(translations, 'MainContentSteps.mainContentStepTwo', 'Make a deposit'),
      mainContentDescrTwoDescr: _.get(translations, 'MainContentSteps.mainContentStepTwoDescr', 'The minimum amount is $/€ 10'),
      mainContentDescrThree: _.get(translations, 'MainContentSteps.mainContentStepThree', 'Receive a 100% Bonus'),
      mainContentDescrThreeDescr: _.get(translations, 'MainContentSteps.mainContentStepThreeDescr', 'Increase your deposit up to $/€ 555'),
      achievmentOneHeader: _.get(translations, 'AchievmentsFields.achievmentOneHeader', 'Over 65'),
      achievmentOneDescr: _.get(translations, 'AchievmentsFields.achievmentOneDescr', 'sports'),
      achievmentTwoHeader: _.get(translations, 'AchievmentsFields.achievmentTwoHeader', 'Over 7000'),
      achievmentTwoDescr: _.get(translations, 'AchievmentsFields.achievmentTwoDescr', 'sports leagues'),
      achievmentThreeHeader: _.get(translations, 'AchievmentsFields.achievmentThreeHeader', 'Over 30,000'),
      achievmentThreeDescr: _.get(translations, 'AchievmentsFields.achievmentThreeDescr', 'events each month'),
      achievmentThreeDescr2: _.get(translations, 'AchievmentsFields.achievmentThreeDescr2', 'in Live mode'),

    };
    const template = document.getElementById('template').innerHTML;
    const compiledTemplate = Handlebars.compile(template);
    const complete = compiledTemplate(data);
    document.getElementById('target').innerHTML = complete;
  });
function SendForm() {
  const that = this;

  this.clearErrors = function () {
    console.log('this', this);
    document.querySelector('.form__input-wrapper').classList.remove('form__input-error');
    document.getElementsByClassName('form__terms-label-error__text')[0].innerHTML = '';
    document.getElementsByClassName('form__input-error__text')[0].innerHTML = '';
    const errorCheckbox = document.querySelector('.form__terms-label-error__text');
    errorCheckbox.classList.remove('form__terms-label-error');
  };

  this.send = async () => {
    const regButton = document.getElementById('reg-button');
    const agreeCheckbox = document.getElementById('checkbox');
    const emailInput = document.getElementById('email');
    if (regButton.disabled) return;
    // отключаем кнопку
    regButton.disabled = true;

    const r = await register({
      email: emailInput.value,
      agreed: agreeCheckbox.checked,
      source: 'sports-landing',
    });

    that.clearErrors();
    this.clearErrors();

    regButton.disabled = false;

    if (r.result === 'error') {
      const errorCheckbox = document.querySelector('.form__terms-label-error__text');
      const { errors } = r;
      if (errors.agreed) {
        document.getElementsByClassName('form__terms-label-error__text')[0].innerHTML = errors.agreed.join('; ');
        errorCheckbox.classList.add('form__terms-label-error');
      }
      if (errors.email) {
        const mailField = document.querySelector('input[type=email]');
        const emailFieldParent = document.querySelector('.form__input-wrapper');
        emailFieldParent.classList.add('form__input-error');
        document.querySelector('.form__input-error__text').classList.add('form__terms-label-error');
        document.getElementsByClassName('form__input-error__text')[0].innerHTML = errors.email.join('; ');
        mailField.style.border = '2px solid #ff0000';
      }
    } else {
      modal.classList.add('open');
      document.querySelector('.modal-email').innerHTML = emailInput.value;
      getRegisterHref();
    }
  };
}
window.sendForm = new SendForm();

console.log('It works');

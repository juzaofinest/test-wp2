const headTemplate = require('!!raw-loader!./../common-templates/scripts.html')
  .replace('{{OPTIMIZE_ID}}', process.env.OPTIMIZE_ID);

module.exports = process.env.NODE_ENV === 'production' ? headTemplate : '';

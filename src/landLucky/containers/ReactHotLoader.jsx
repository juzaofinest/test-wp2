import React from 'react';
import { AppContainer } from 'react-hot-loader';

// We create this wrapper so that we only import react-hot-loader for a
// development build.  Small savings. :)
const ReactHotLoader = process.env.NODE_ENV === 'development'
  ? AppContainer
  : ({ children }) => React.Children.only(children);

export default ReactHotLoader;

// @flow
import React from 'react';
import Polyglot from 'node-polyglot';
import PropTypes from 'prop-types';
import { withContext } from 'recompose';
import App from './../app';
import getTranslations, { defaultTranslations } from './../../getTranslations';

const translate = withContext(
  { translate:PropTypes.func.isRequired },
  props => (props),
);
const TranslatedApp = translate(App);

// const delay = time => res => new Promise(resolve => {
//   setTimeout(() => {
//     resolve(res);
//   }, time);
// });

export default class Bootloader extends React.PureComponent<*, {
  polyglot: Polyglot,
}> {
  state = {
    polyglot: new Polyglot(),
  };

  async componentWillMount() {
    const { polyglot } = this.state;
    polyglot.t = polyglot.t.bind(polyglot);
    polyglot.extend(defaultTranslations);
    const translations = await getTranslations(); // .then(delay(1500));
    polyglot.extend(translations);
    this.forceUpdate();
  }

  render() {
    return <TranslatedApp translate={this.state.polyglot.t} />;
  }
}

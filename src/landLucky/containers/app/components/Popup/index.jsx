// @flow
import React from 'react';
import { Transition } from 'react-transition-group';
import nl2br from 'react-nl2br';
import queryString from 'query-string';
import Form from './components/Form';
import { translateEnhance } from './../../../../getTranslations';

import './style.styl';
import type { Dispatch, Prize } from './../../redux/actions';

type PopupProps = {
  +prizes: Array<Prize>,
  +visible: boolean,
  +dispatch: Dispatch,
  +translate: string => string,
};

const defaultStyle = {
  transition: `opacity 300ms ease-in-out, transform 500ms linear`,
  opacity: 0,
};

const transitionStyles = {
  entering: { opacity:0 },
  entered: { opacity:1 },
};

const modalDefaultStyle = {
  willChange: 'transform',
  transition: `opacity 400ms ease-in-out, transform 500ms cubic-bezier(0.175, 0.885, 0.32, 1.275)`,
  opacity: 0.5,
  transform: 'scale(0)',
};

const modalTransitionStyles = {
  entering: { opacity:0 },
  entered: {
    opacity: 1,
    transform: 'scale(1)',
  },
};

const getRegisterHref = (translate: string => string): string => {
  const lang = [ 'ru', 'en', 'tr', 'uk', 'ka' ].includes(translate('language')) ? translate('language') : 'en';
  const params = [ 'cid', 'postback', 'utm_term' ];
  const searchPrams = queryString.parse(location.search);
  let paramsString = params.map(p => {
    const val = searchPrams[p];
    return val ? `${p}=${val}` : '';
  }).filter(Boolean).join('&');

  if (paramsString.length) {
    paramsString = `?${paramsString}`;
  }

  return `https://yourbet.com/${lang}/register${paramsString}`;
};

const Popup = ({
  prizes,
  visible,
  dispatch,
  translate,
}: PopupProps) => {
  const count = prizes.length;
  return (
    <Transition in={visible} timeout={500}>
      {state => count > 0 && (
        <div
          className={`modal ${state}`}
          style={{
            visibility: state === 'exited' ? 'hidden' : 'visible',
            ...defaultStyle,
            ...transitionStyles[state],
          }}
        >
          <div className="modal-overlay" />
          <div className="modal-scroll">
            <div className="modal-center">
              <div className="modal-center-row">
                <div className="modal-center-cell">
                  <div className="modal-content" role="document">
                    <div className="popup__stars" />
                    <div
                      className="popup"
                      style={{
                        ...modalDefaultStyle,
                        ...modalTransitionStyles[state],
                      }}
                    >
                      {[ 2, 3 ].includes(count) && <div className="popup__badge"><span>{nl2br(translate(`Popup.badge${count}`))}</span></div>}
                      <div className="popup__title">{translate(`Popup.title${count}`)}</div>
                      <div className="popup__desc">{nl2br(translate(`Popup.desc${count}`))}</div>
                      <div className="popup__prizes">
                        {prizes.map(prize => (
                          <div key={prize.url} className="popup__prize">
                            <div
                              className="popup__prize-img"
                              style={{
                                background: `url(${prize.url}) center / contain no-repeat`,
                              }}
                            />

                            <div className="popup__prize-title" data-text={translate(prize.title)} >
                              {translate(prize.title)}
                            </div>
                            <div className="popup__prize-desc">
                              {nl2br(translate(prize.desc))}
                            </div>
                          </div>
                        ))}
                      </div>
                      
                      <Form
                        translate={translate}
                        href={getRegisterHref(translate)}
                      />

                      {count < 3 && (
                        <div className="popup__btn-wrapper">
                          <button
                            className="popup__btn-more"
                            onClick={() => {
                              dispatch({
                                type: "CLOSE_POPUP",
                              });
                              // uncomment to spin after close
                              // const btn = document.querySelector('#startButton');
                              // if (btn) btn.click();
                            }}
                          >{translate('Popup.btnMore')}</button>
                        </div>
                      )}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      )}
    </Transition>
  );
};

Popup.defaultProps = {
  prizes: [],
};

export default translateEnhance(Popup);

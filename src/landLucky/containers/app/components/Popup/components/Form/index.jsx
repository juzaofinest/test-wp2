// @flow
/* eslint-disable no-confusing-arrow */
import React from 'react';
import { Form, Text, Checkbox } from 'react-form';
import _ from 'lodash';
import { withStateHandlers } from 'recompose';
import { register, type RegisterFormValues } from '_api/register';

import './style.styl';
import * as gtmEvents from './../../../../gtmEvents';

let fa = null;

const RegForm = ({
  translate,
  isLoading = false,
  setIsLoading,
  success,
  setSuccess,
  href,
}: {
  translate: (string) => string,
  href: string,
  isLoading: boolean,
  setIsLoading: (boolean) => any,
  success: ?boolean,
  setSuccess: (boolean) => any,
}) => !success ? (
  <Form
    onSubmit={async (values: RegisterFormValues) => {
      setIsLoading(true);
      const res = await register({
        ...values,
        source: 'slotmachine-landing',
      });

      console.log('>>>>>', res);
      setIsLoading(false);

      if (res.result === 'ok') {
        setSuccess(true);
        gtmEvents.formSuccess();
      } else {
        if (fa) {
          console.log(fa.getFormState());
          fa.setFormState({
            ...fa.getFormState(),
            errors: res.errors,
          });
        }
        alert(Object.values(res.errors).join('\n'));
      }
    }}
  >
    {(formApi) => {
      fa = formApi; return (
        <form onSubmit={formApi.submitForm} id="form1" className="form-register">
          <label className="form-register__label" htmlFor="email">
            <Text
              field="email"
              id="email"
              className={[
                'form-register__text-input',
                !!_.get(formApi, 'errors.email') && 'has-error',
              ].filter(Boolean).join(' ')}
              validate={value => (!value || !(/\w@\w+\.\w/.test(value))) && 'required'}
              placeholder="E-mail"
              onChange={() => {
                formApi.setTouched('email', true);
              }}
            />
            {/* <span>E-mail</span> */}
          </label>
          <label className="form-register__checkbox-label" htmlFor="agreed">
            <Checkbox
              field="agreed"
              id="agreed"
              className={[
                'form-register__checkbox',
                !!_.get(formApi, 'errors.agreed') && 'has-error',
              ].filter(Boolean).join(' ')}
              validate={value => (!value) && 'required'}
            />
            <span>
              {translate('TandC.iAgreeWith')}
              {' '}
              <a href="https://yourbet.com/en/pages/terms-and-conditions">
                {translate('TandC.termsAndConditions')}
              </a>
              {' '}
              {translate('TandC.and')}
              {' '}
              <a href="https://yourbet.com/en/pages/privacy-policy">
                {translate('TandC.privacyPolicy')}
              </a>
              {'. '}
              {translate('TandC.notACitizen')}
            </span>
          </label>
          <div className="popup__btn-wrapper">
            <button
              className="popup__btn"
              type="submit"
              disabled={isLoading}
            >{translate('Popup.btnRegister')}
            </button>
          </div>
        </form>
      );
    }}
  </Form>
) : (
  <div className="success-block">
    <div className="success-block__heading">{translate('Popup.checkYourEmail')}</div>
    <a
      className="popup__btn"
      href={href}
    >OK
    </a>
  </div>
);

export default withStateHandlers(
  { isLoading:false, success:null },
  {
    setIsLoading: () => val => ({ isLoading:val }),
    setSuccess: () => val => ({ success:val }),
  },
)(RegForm);

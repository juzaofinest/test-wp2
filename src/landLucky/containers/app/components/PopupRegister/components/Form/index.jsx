// @flow
import React from 'react';
import { Form, Text } from 'react-form';


export default ({ translate }: {
  translate: (string) => string,
}) => (
  <Form
    onSubmit={console.log}
  >
    {formApi => (
      <form onSubmit={formApi.submitForm} id="form1" className="mb-4">
        <div className="popup__title">Залупа</div>
        <label htmlFor="hello">Hello World
          <Text field="hello" id="hello" />
        </label>
        <div className="popup__btn-wrapper">
          <button
            className="popup__btn"
            type="submit"
          >{translate('Popup.btnRegister')}</button>
        </div>
      </form>
    )}
  </Form>
);

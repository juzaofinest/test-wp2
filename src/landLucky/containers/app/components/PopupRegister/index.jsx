// @flow
import React from 'react';
import { Transition } from 'react-transition-group';
import Form from './components/Form';
import { translateEnhance } from './../../../../getTranslations';

import './style.styl';
import type { Dispatch, Prize } from './../../redux/actions';

type PopupProps = {
  +prizes: Array<Prize>,
  +visible: boolean,
  +dispatch: Dispatch,
  +translate: string => string,
};

const defaultStyle = {
  transition: `opacity 300ms ease-in-out, transform 500ms linear`,
  opacity: 0,
};

const transitionStyles = {
  entering: { opacity:0 },
  entered: { opacity:1 },
};

const modalDefaultStyle = {
  willChange: 'transform',
  transition: `opacity 400ms ease-in-out, transform 500ms cubic-bezier(0.175, 0.885, 0.32, 1.275)`,
  opacity: 0.5,
  transform: 'scale(0)',
};

const modalTransitionStyles = {
  entering: { opacity:0 },
  entered: {
    opacity: 1,
    transform: 'scale(1)',
  },
};


const PopupRegister = ({
  prizes,
  visible,
  dispatch,
  translate,
}: PopupProps) => {
  const count = prizes.length;
  return (
    <Transition in={visible} timeout={500}>
      {state => count > 0 && (
        <div
          className={`register-modal ${state}`}
          style={{
            visibility: state === 'exited' ? 'hidden' : 'visible',
            ...defaultStyle,
            ...transitionStyles[state],
          }}
        >
          <div className="modal-overlay" />
          <div className="modal-scroll">
            <div className="modal-center">
              <div className="modal-center-row">
                <div className="modal-center-cell">
                  <div className="modal-content" role="document">
                    <div
                      className="popup"
                      style={{
                        ...modalDefaultStyle,
                        ...modalTransitionStyles[state],
                      }}
                    >
                      <Form translate={translate} />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      )}
    </Transition>
  );
};

PopupRegister.defaultProps = {
  prizes: [],
};

export default translateEnhance(PopupRegister);

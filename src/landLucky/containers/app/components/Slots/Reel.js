// @flow
import Konva from 'konva';

import type { SlotImage } from './utils';

const slotWidth = 150;
const slotHeight = 112;

const forceRedraw = (element: HTMLElement): number => {
  const disp = element.style.display;
  // eslint-disable-next-line no-param-reassign
  element.style.display = 'none';
  const trick = element.offsetHeight;
  // eslint-disable-next-line no-param-reassign
  element.style.display = disp;
  return trick;
};

export default class Reel {
  layer: Konva.Layer;
  canvas: HTMLElement;
  time = 0;
  images: Array<SlotImage>;
  state: $ReadOnly<{ phase: 'created'}
    | { phase: 'inited' }
    | { phase: 'spinning', duration: number, }
    | { phase: 'slowing', idToStop: $PropertyType<SlotImage, 'id'>, duration: number, toDuration: number }
    | { phase: 'stopping', idToStop: $PropertyType<SlotImage, 'id'>, duration: number }
    | { phase: 'stopped' }>
  = { phase:'created' };
  imagePositions: WeakMap<Konva.Image, number> = new WeakMap();
  imageMap: WeakMap<Konva.Image, SlotImage> = new WeakMap();
  slots: Array<Konva.Image> = [];
  stopCallback = () => {};

  constructor(canvas: HTMLElement, images: Array<SlotImage>, stopCallback: ?Function) {
    this.canvas = canvas;
    this.images = images;
    if (!(canvas instanceof HTMLElement)) throw new Error('canvas not defined');
    if (!(images.length)) throw new Error('images not defined');
    if (stopCallback instanceof Function) this.stopCallback = stopCallback;
  }

  init(centerImage: ?SlotImage) {
    const stage = new Konva.Stage({
      container: this.canvas,
      width: 1 * slotWidth,
      height: 5 * slotHeight,
    });

    this.layer = new Konva.Layer();

    for (let i = 0; i <= 4; i += 1) {
      let image = null;
      if (i === 2 && centerImage instanceof Object) {
        image = centerImage;
      } else {
        image = this.getRandomImage();
      }
      const slot = new Konva.Image({
        x: 0,
        y: i * slotHeight,
        image: image.image,
        width: slotWidth,
        height: slotHeight,
      });
      this.layer.add(slot);
      this.imagePositions.set(slot, i);
      this.imageMap.set(slot, image);
      this.slots.push(slot);
    }
    stage.add(this.layer);
    this.state = { phase:'inited' };
    setTimeout(() => {
      // $FlowFixMe
      this.canvas.querySelector('.konvajs-content').style.height = null;
      // $FlowFixMe
      this.canvas.querySelector('.konvajs-content').style.width = '100%';
      // $FlowFixMe
      this.canvas.querySelector('.konvajs-content').style.position = 'absolute';
      // $FlowFixMe
      this.canvas.querySelector('.konvajs-content canvas').style.height = null;
      // $FlowFixMe
      this.canvas.querySelector('.konvajs-content canvas').style.width = '100%';
      // $FlowFixMe
      this.canvas.querySelector('.konvajs-content canvas').style.top = '50%';
      forceRedraw(this.canvas);
    }, 10);
  }
  getRandomImage(): SlotImage {
    return this.images[Math.floor(Math.random() * this.images.length)];
  }

  startSpin = () => {
    if (this.state.phase !== 'inited' && this.state.phase !== 'stopped') {return;}
    this.state = { phase:'spinning', duration:(0.05 + Math.random() * 0.03) };
    this.spin();
  }

  spin = () => {
    if (this.state.phase !== 'slowing' && this.state.phase !== 'spinning' && this.state.phase !== 'stopping') {return;}

    const recalculatePositions = () => {
      // alert();
      this.layer.y(0);
      this.slots.forEach(slot => {
        const position = this.imagePositions.get(slot);
        if (position === 4) {
          this.imagePositions.set(slot, 0);
          const image = this.getRandomImage();
          slot.y(0);
          slot.image(image.image);
          this.imageMap.set(slot, image);
        } else {
          this.imagePositions.set(slot, position + 1);
          slot.y((position + 1) * slotHeight);
        }
      });
    };
    if (this.state.phase === 'slowing') {
      const { toDuration, idToStop, duration } = this.state;
      const newDuration = Math.min(duration * 1.15, toDuration);
      if (newDuration === this.state.toDuration) {
        this.state = {
          phase: 'stopping',
          idToStop,
          duration: newDuration,
        };
      } else {
        this.state = {
          phase: 'slowing',
          idToStop,
          toDuration,
          duration: newDuration,
        };
      }
    }
    this.layer.to({
      y: slotHeight,
      duration: this.state.duration,
      onFinish: () => {
        recalculatePositions();
        if (this.state.phase === 'stopping' && this.needsToStop(this.state.idToStop)) {
          this.animateStop().then(() => {
            this.state = { phase:'stopped' };
            this.stopCallback();
          });
        } else {
          this.spin();
        }
      },
    });
  };

  getCurrentItem: () => ?SlotImage = () => {
    for (const slot of this.slots) {
      if (this.imagePositions.get(slot) === 2) {
        const si = this.imageMap.get(slot);
        if (!si) throw new Error('');
        return si;
      }
    }
  };

  needsToStop: ($PropertyType<SlotImage, 'id'>) => boolean = (idToStop) => {
    const ci = this.getCurrentItem();
    if (!ci) return false;
    if (ci.id === idToStop) return true;
    return false;
  }

  animateStop: () => Promise<true> = () => new Promise(resolve => {
    const amplitude = slotHeight / 3;
    const period = 600; // in ms
    const anim = new Konva.Animation((frame) => {
      this.layer.setY((frame.time > period - 16) ? 0 : amplitude * Math.sin(frame.time * 2 * Math.PI / period));
    }, this.layer);
    anim.start();
    setTimeout(() => {
      anim.stop();
      this.layer.y(0);
      resolve(true);
    }, period);
  });

  stopAt: ($PropertyType<SlotImage, 'id'>) => Promise<$PropertyType<SlotImage, 'id'>>
  = (item) => new Promise(resolve => {
    if (this.state.phase !== 'spinning') return;
    this.state = {
      phase: 'slowing',
      duration: this.state.duration,
      toDuration: .2,
      idToStop: item,
    };
    const interval = setInterval(() => {
      if (this.state.phase === 'stopped') {
        resolve(item);
        clearInterval(interval);
      }
    }, 100);
  });

}

  // @flow
import _ from 'lodash';
import Reel from './Reel';

import type { SlotImage } from './utils';
import { possiblePrizes } from './utils';

type PrizeId = $PropertyType<SlotImage, 'id'>;

function* generateSequence(): Generator<?PrizeId, void, void> {
  const prizes = _.shuffle(possiblePrizes);
  while (prizes.length) {
    yield Math.random() > 1 ? null : prizes.pop();
  }
  while (!prizes.length) {
    yield Math.random() > 0.3 ? null : _.sample(possiblePrizes);
  }
}

const getReelLayout = (prize: ?PrizeId): Array<PrizeId> => {
  if (prize) return _.fill(Array(3), prize);
  const layout = _.fill(Array(3), prize).map(() => _.sample(possiblePrizes));
  while (_.uniq(layout).length < 2) {
    layout.splice(1, 1, _.sample(possiblePrizes));
  }
  return layout;
};


export default class Game {
  reels: Map<number, Reel> = new Map();
  sequence: Generator<?PrizeId, void, void>;

  constructor(canvas: Array<HTMLElement>, images: Array<SlotImage>) {
    for (let i = 0; i < canvas.length ; i += 1) {
      const reel = new Reel(canvas[i], images);
      this.reels.set(i + 1, reel);
      reel.init(images[i % 3]);
    }
    this.sequence = generateSequence();
  }

  start: () => Promise<?PrizeId> = async () => {
    const value = this.sequence.next().value;
    const stopLayout = getReelLayout(value);
    await Promise.all(Array.from(this.reels.values()).map(async (reel, index) => {
      reel.startSpin();
      await new Promise(r => setTimeout(r, 1000 + index * 1500));
      return reel.stopAt(stopLayout[index]);
    }));
    const uniq = _.uniq(stopLayout);
    if (uniq.length === 1) return uniq.pop();
  }

}

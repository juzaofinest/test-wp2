// @flow
import React from 'react';
import _ from 'lodash';

import './style.styl';
import slotImage1 from './images/slots1.png';
import slotImage2 from './images/slots2.png';
import slotImage3 from './images/slots3.png';

import Reel from './Reel';
import Game from './game';
import { preloadImages } from './utils';
import type { Dispatch, Prize } from './../../redux/actions';
import type { AppState } from './../../redux/reducer';
import { addPrize } from './../../redux/actions';
import { translateEnhance } from './../../../../getTranslations';

class Slots extends React.Component<{
  dispatch: Dispatch,
  appState: AppState,
  translate: string => string,
}, {|
  spinning: boolean,
|}> {

  state = {
    spinning: false,
  };
  async componentDidMount() {
    this.prizes = [
      {
        id: 'bonus200',
        title: 'Prizes.bonus200.title',
        shortTitle: 'Prizes.bonus200.shortTitle',
        desc: 'Prizes.bonus200.desc',
        url: slotImage2,
      }, {
        id: 'freespins',
        title: 'Prizes.freespins.title',
        shortTitle: 'Prizes.freespins.shortTitle',
        desc: 'Prizes.freespins.desc',
        url: slotImage3,
      }, {
        id: 'bonus50',
        title: 'Prizes.bonus50.title',
        shortTitle: 'Prizes.bonus50.shortTitle',
        desc: 'Prizes.bonus50.desc',
        url: slotImage1,
      },
    ];
    
    const images = _.shuffle(await preloadImages());

    if (this.canvas1 instanceof HTMLElement
      && this.canvas2 instanceof HTMLElement
      && this.canvas3 instanceof HTMLElement
    ) {
      this.game = new Game([
        this.canvas1,
        this.canvas2,
        this.canvas3,
      ], images);
    }
  }

  // componentWillReceiveProps(props) {
  //   console.log(this.props.translate('Prizes.bonus200.title'));
  //   console.log(props.translate('Prizes.bonus200.title'));
  // }

  game: Game;
  reel1: Reel;
  reel2: Reel;
  reel3: Reel;
  canvas1: ?HTMLElement;
  canvas2: ?HTMLElement;
  canvas3: ?HTMLElement;
  prizes: Array<Prize> = [];

  start = async () => {
    if (this.state.spinning) return;
    await new Promise(resolve => this.setState({ spinning:true }, resolve));
    const prizeId = await this.game.start();
    this.setState({ spinning:false });
    if (!prizeId) return;
    const { appState, dispatch } = this.props;
    if (_.find(appState.prizes, { id:prizeId })) return;

    dispatch(addPrize(_.find(this.prizes, { id:prizeId })));
    dispatch({ type:"OPEN_POPUP" });
  }

  render() {
    const { dispatch, translate } = this.props;
    return (
      <div className="slots">
        <div className="slots__game-wrapper">
          <div className="slots__game" id="canvas">
            <div className="slots__shadow" />
            <div className="slots__reels" >
              <div className="slots__reel-spacer" />
              <div className="slots__reel" ref={el => { this.canvas1 = el; }}>&nbsp;</div>
              <div className="slots__reel-spacer" />
              <div className="slots__reel" ref={el => { this.canvas2 = el; }}>&nbsp;</div>
              <div className="slots__reel-spacer" />
              <div className="slots__reel" ref={el => { this.canvas3 = el; }}>&nbsp;</div>
              <div className="slots__reel-spacer" />
            </div>
          </div>
        </div>
        <button
          id="startButton"
          className="slots__button"
          onClick={this.start}
          disabled={this.state.spinning}
        >{translate('Slots.button')}</button>
        {process.env.DEBUG === 'true' && <button
          className="slots__button"
          onClick={() => {
            dispatch(addPrize(_.sample(this.prizes)));
            dispatch({
              type: "OPEN_POPUP",
            });
          }}
        >приз!</button>}
      </div>
    );
  }
}

export default translateEnhance(Slots);

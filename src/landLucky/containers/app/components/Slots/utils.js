// @flow
import slotImage1 from './images/slots1.png';
import slotImage2 from './images/slots2.png';
import slotImage3 from './images/slots3.png';
// const slotImage3 = './images/slots3.png';

const IMAGE_HEIGHT = 64;
const IMAGE_TOP_MARGIN = 5;
const IMAGE_BOTTOM_MARGIN = 5;
const SLOT_SEPARATOR_HEIGHT = 2;
const SLOT_HEIGHT = IMAGE_HEIGHT + IMAGE_TOP_MARGIN + IMAGE_BOTTOM_MARGIN + SLOT_SEPARATOR_HEIGHT; // how many pixels one slot image takes
// const ITEM_COUNT = 3; // item count in slots


export type SlotImage = {|
  id: 'bonus50' | 'bonus200' | 'freespins',
  url: string,
  image: Image,
|};

export const possiblePrizes = [ 'bonus50', 'bonus200', 'freespins' ];

export const preloadImages = async (): Promise<Array<SlotImage>> => {
  const images = [
    { id:'bonus50', url:slotImage1 },
    { id:'bonus200', url:slotImage2 },
    { id:'freespins', url:slotImage3 },
  ];
  const preload = async (asset: $Rest<SlotImage, {| image: Image |}>): Promise<SlotImage> => new Promise((resolve, reject) => {
    const img = new Image();
    img.src = asset.url;
    img.addEventListener("load", () => {
      const a = ({
        id: asset.id,
        url: asset.url,
        image: img,
      });

      resolve(a);
    }, false);
    img.addEventListener("error", () => {
      reject();
    }, false);
  });

  const htmlImages = await Promise.all(images.map(preload));

  return htmlImages;
};


export const fillCanvas = (canvas: HTMLCanvasElement, items: Array<SlotImage>) => {
  const ctx = canvas.getContext('2d');
  ctx.fillStyle = '#ddd';

  for (let i = 0 ; i < items.length ; i += 1) {
    const asset = items[i];
    ctx.save();
    ctx.shadowColor = "rgba(0,0,0,0.5)";
    ctx.shadowOffsetX = 5;
    ctx.shadowOffsetY = 5;
    ctx.shadowBlur = 5;
    ctx.drawImage(asset.image, 3, i * SLOT_HEIGHT + IMAGE_TOP_MARGIN);
    ctx.drawImage(asset.image, 3, (i + items.length) * SLOT_HEIGHT + IMAGE_TOP_MARGIN);
    ctx.restore();
    ctx.fillRect(0, i * SLOT_HEIGHT, 70, SLOT_SEPARATOR_HEIGHT);
    ctx.fillRect(0, (i + items.length) * SLOT_HEIGHT, 70, SLOT_SEPARATOR_HEIGHT);
  }
};

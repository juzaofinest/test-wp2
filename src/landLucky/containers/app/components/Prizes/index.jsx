// @flow
import React from 'react';
// import { branch, renderComponent } from 'recompose';

import './style.styl';
// import type { SlotImage } from './../Slots/utils';
import type { Prize } from './../../redux/actions';
import img from './../Slots/images/slots1.png';
import { translateEnhance } from './../../../../getTranslations';

type PrizesProps = {
  prizes: Array<Prize>,
  translate: string => string,
};

const Prizes = ({ prizes = [], translate }: PrizesProps) => {
  if (!prizes.length) return null;
  return (
    <div className="yours">
      <div className="yours__title">{translate('Prizes.title')}</div>
      {prizes.map(p => (
        <div key={p.id} className="yours__prize">
          <img src={p.url} alt={p.id} />
          <div className="yours__desc">{translate(p.shortTitle)}</div>
        </div>
      ))}
    </div>
  );
};

export default translateEnhance(Prizes);

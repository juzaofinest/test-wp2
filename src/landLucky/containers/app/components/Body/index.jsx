// @flow
import React from 'react';

import Slots from './../Slots';
import Prizes from './../Prizes';
import './style.styl';
import type { AppState } from './../../redux/reducer';
import type { Dispatch } from './../../redux/actions';
import { translateEnhance } from './../../../../getTranslations';

const Body = ({
  className,
  appState,
  dispatch,
  translate,
}: {
  className: string,
  appState: AppState,
  dispatch: Dispatch,
  translate: (string) => string,
}) => (
  <div className={className}>
    <Prizes prizes={appState.prizes} />
    <div className="logo" />
    <div className="luck">{translate("Body.luck")}</div>
    <Slots dispatch={dispatch} appState={appState} />
    <div className="footnote">{translate("Body.footnote")}</div>
  </div>
);


export default translateEnhance(Body);

// @flow
import React from 'react';
import { withReducer } from 'recompose';

import Head from './components/Head';
import Body from './components/Body';
import Popup from './components/Popup';
import reducer, { initial } from './redux/reducer';
import type { AppState } from './redux/reducer';
import type { Dispatch } from './redux/actions';
import './style.styl';


const App = ({
  appState,
  dispatch,
}: {
  appState: AppState,
  dispatch: Dispatch,
}) => (
  <div className="app">
    <Head className="app__head" />
    <Body className="app__body" {...{ appState, dispatch }} />
    <Head className="app__tail" />
    <Popup
      visible={appState.popupVisible}
      dispatch={dispatch}
      prizes={appState.prizes}
    />
  </div>
);

export default withReducer(
  'appState',
  'dispatch',
  reducer,
  initial,
)(App);

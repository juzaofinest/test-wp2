// @flow
import type { SlotImage } from './../components/Slots/utils';


type OpenPopupAction = {
  +type: "OPEN_POPUP",
};
export const openPopup:OpenPopupAction = {
  type: "OPEN_POPUP",
};

type ClosePopupAction = {
  +type: "CLOSE_POPUP",
};
export const closePopup:ClosePopupAction = {
  type: "CLOSE_POPUP",
};

type OpenRegisterPopupAction = {
  +type: "OPEN_POPUP_REGISTER",
};
export const openRegisterPopup: OpenRegisterPopupAction = {
  type: "OPEN_POPUP_REGISTER",
};

type CloseRegisterPopupAction = {
  +type: "CLOSE_POPUP_REGISTER",
};
export const closeRegisterPopup:CloseRegisterPopupAction = {
  type: "CLOSE_POPUP_REGISTER",
};

// type UserRegisterSuccessAction = {
//   +type: "USER_REGISTER_SUCCESS",
// };

export type Prize = {
  id: $PropertyType<SlotImage, 'id'>,
  url: string,
  title: string,
  shortTitle: string,
  desc: string,
};

type AddPrizeAction = {
  +type: "ADD_PRIZE",
  +prize: Prize,
};
export const addPrize: (Prize) => AddPrizeAction = (prize) => ({
  type: "ADD_PRIZE",
  prize,
});

export type Action =
  | OpenPopupAction
  | ClosePopupAction
  | OpenRegisterPopupAction
  | CloseRegisterPopupAction
  | AddPrizeAction
;

export type Dispatch = (Action) => any;


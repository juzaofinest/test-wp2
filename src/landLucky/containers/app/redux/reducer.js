// @flow
import type { Action, Prize } from './actions';

export type AppState = {
  popupVisible: boolean,
  popupRegisterVisible: boolean,
  prizes: Array<Prize>,
};
  
export const initial: AppState = {
  popupVisible: false,
  popupRegisterVisible: false,
  prizes: [],
};


export default (state: AppState = initial, action: Action): AppState & $Shape<AppState> => {
  switch (action.type) {
  case "OPEN_POPUP": {
    if (document.body) document.body.style.overflow = 'hidden';
    console.log('open');
    return {
      ...state,
      popupVisible: true,
    };
  }
  case "CLOSE_POPUP": {
    if (document.body) document.body.style.overflow = 'auto';
    return {
      ...state,
      popupVisible: false,
    };
  }
  case "OPEN_POPUP_REGISTER": {
    return {
      ...state,
      popupRegisterVisible: true,
    };
  }
  case "CLOSE_POPUP_REGISTER": {
    return {
      ...state,
      popupRegisterVisible: false,
    };
  }
  case "ADD_PRIZE": {
    const { prize } = action;
    const prizes = state.prizes.filter(p => p.id !== prize.id);
    prizes.push(prize);
    return {
      ...state,
      prizes,
    };
  }
  default:
    return state;
  }
};

// @flow

const getDataLayer = () => window.dataLayer || ({
  push: console.log,
});

let alreadyAgreed = false;
export const agreed = () => {
  if (alreadyAgreed) return;
  alreadyAgreed = true;
  getDataLayer().push({
    event: 'luckulanding_agreed',
  });
};

export const registerPushed = () => {
  getDataLayer().push({
    event: 'luckulanding_reg_button',
  });
};

export const formSuccess = () => {
  getDataLayer().push({
    event: 'luckulanding_form_success',
  });
};

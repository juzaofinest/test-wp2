// @flow
import PropTypes from 'prop-types';
import { getContext } from 'recompose';

export const defaultTranslations = {
  language: 'default',
  Body: {
    luck: 'Test your luck',
    footnote: 'Well-tried entertainment - guaranteed winnings',
  },
  Slots: {
    button: 'Push',
  },
  Prizes: {
    title: 'This is yours:',
    freespins: {
      title: 'Free spins',
      shortTitle: 'Up to 100 freespins',
      desc: 'Up to 100 freespins',
    },
    bonus50: {
      title: 'Bonus +50%',
      shortTitle: 'Win and get +50%',
      desc: 'Win and get +50%',
    },
    bonus200: {
      title: 'Bonus up to 200%',
      shortTitle: '+200% on deposit',
      desc: 'Up to 1000$/€',
    },
  },
  Popup: {
    title1: 'You\'ve won the bonus!',
    desc1: 'Make a deposit and get your bonus',
    title2: 'Now you have 2 bonuses!',
    desc2: 'Make a deposit and get your bonus',
    title3: 'Wow! You have 3 bonuses!',
    desc3: 'You\'ve hit the jackpot!',
    btnRegister: 'Register now',
    btnMore: 'I want more bonuses',
    badge2: 'You are so lucky!',
    badge3: 'You\'re fucking lucky!',
  },
};


export default async (): Promise<{}> => {
  try {
    return await fetch(`${process.env.LANDINGS_API_URL || ''}/translations/novice${window.location.search}`, {
      credentials: 'include',
      headers: {
        Accept: 'application/json, text/plain, */*',
        'Content-Type': 'application/json',
      },
    }).then(r => r.json());
    // .then(r => resolve(r));
    // throw new Error();
  } catch (err) {
    return defaultTranslations;
  }
};

export const translateEnhance = getContext({
  translate: PropTypes.func.isRequired,
});


// @flow
import React from 'react';
import { render } from 'react-dom';
import 'reset.css';
import 'normalize.css';

import AppContainer from './containers/ReactHotLoader';
import Bootloader from './containers/bootloader';

import './style.styl';
import '../Fonts/PFDinDisplayPro/stylesheet.css';

const container = document.getElementById('app');

if (container) {
  const renderApp = Component => render(
    (
      <AppContainer>
        {/* $FlowFixMe */}
        <Component />
      </AppContainer>
    ), container,
  );

  renderApp(Bootloader);

  if (module && module.hot) {
    // Accept changes to this file for hot reloading.
    // $FlowFixMe
    module.hot.accept('./index.jsx');

    // Any changes to our App will cause a hotload re-render.
    // $FlowFixMe
    module.hot.accept('./containers/bootloader', async () => {
      renderApp(await import(/* webpackChunkName: "bootloader.container" */ './containers/bootloader'));
    });
  }
}

console.log('it Works');

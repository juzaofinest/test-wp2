/* eslint-disable func-names */
import Handlebars from 'handlebars/dist/handlebars';
import _ from 'lodash';
import queryString from 'query-string';

import 'reset.css';
import 'normalize.css';
import './checkbox.css';

// import 'picturefill';

import './style.styl';
import '../Fonts/Glober/stylesheet.css';

import { getTranslations, register } from '../api';

// console.log(process.env.OPTIMIZE_ID);
const transUrl = `${process.env.LANDINGS_API_URL || ''}/translations/cyberpunk${window.location.search}`;
// const mailField = document.querySelector("input[type=email]");
// // const redirectButton = document.getElementById('redirect-href');
// const errorMail = document.querySelector('.form__input-error__text');

const getRegisterHref = () => {
  const redirectButton = document.getElementById('redirect-href');
  // const lang = [ 'ru', 'en', 'tr', 'uk', 'ka' ].includes(translate('language')) ? translate('language') : 'en';
  const lang = 'en';
  const params = [ 'cid', 'postback', 'utm_term' ];
  const searchPrams = queryString.parse(location.search);
  let paramsString = params.map((p) => {
    const val = searchPrams[p];
    return val ? `${p}=${val}` : '';
  }).filter(Boolean).join('&');
  if (paramsString.length) {
    paramsString = `?${paramsString}`;
  }
  const returnUrl = `https://yourbet.com/${lang}/register${paramsString}`;
  redirectButton.href = returnUrl;
  return returnUrl;
};


fetch(transUrl, {
  credentials: 'include',
})
  .then(response => response.json())
  .catch(() => {
  })
  .then((translations) => {
    const data = {
      dialogPart1: _.get(translations, 'DialogBubble.dialogPart1', 'Lets play'),
      dialogPart2: _.get(translations, 'DialogBubble.dialogPart2', 'I love a long, long play. And you?!'),
      dialogPart3: _.get(translations, 'DialogBubble.dialogPart3', 'Deposit $/€200 and play with $/€600'),
      dialogPart4: _.get(translations, 'DialogBubble.dialogPart4', 'Or maybe you want extra spins?'),
      modalHeader: _.get(translations, 'Modal.modalHeader', 'Sign up and extend your game'),
      modalAchievementOne: _.get(translations, 'Modal.modalAchievementOne', '+200%'),
      modalAchievementOneDescr: _.get(translations, 'Modal.modalAchievementOneDescr', 'Up to $/€1000'),
      modalAchievementTwo: _.get(translations, 'Modal.modalAchievementTwo', 'Free spins'),
      modalAchievementTwoDescr: _.get(translations, 'Modal.modalAchievementTwoDescr', 'Up to 100 free spins'),
      modalButton: _.get(translations, 'Modal.modalButton', 'Press start'),
      termsIagree: _.get(translations, 'TandC.iAgreeWith', 'I agree with'),
      termsAnd: _.get(translations, 'TandC.and', 'and'),
      termsHref: _.get(translations, 'TandC.termsAndConditions', 'terms and conditions'),
      termsPrivacyPolicy: _.get(translations, 'TandC.privacyPolicy', 'privacy policy'),
      termsCitizen: _.get(translations, 'TandC.notACitizen', 'I am not a citizen of United States of America, Spain, Italy, France, United Kingdom, Russian Federation, Israel, Latvia'),
      emailField: _.get(translations, 'EmailField.emailVerify', 'Your email'),
      popupHeader: _.get(translations, 'PopupField.popupHeader', 'Check your email'),
      popupEmailProof: _.get(translations, 'PopupField.popupEmailProof', 'An invitation to the game was sent to'),
      popupButton: _.get(translations, 'PopupField.popupButton', 'OK, thanks'),
      popupField: _.get(translations, 'PopupField.popupField', 'Please follow the link and enjoy the game!'),
    };
    const template = document.getElementById('template').innerHTML;
    const compiled_template = Handlebars.compile(template);
    const complete = compiled_template(data);
    document.getElementById('target').innerHTML = complete;
  });
function SendForm() {
  const that = this;
  const email = '';

  this.clearErrors = function () {
    console.log('this', this);
    document.querySelector('.form__input-wrapper').classList.remove('form__input-error');
    document.getElementsByClassName('form__terms-label-error__text')[0].innerHTML = '';
    document.getElementsByClassName('form__input-error__text')[0].innerHTML = '';
    const errorCheckbox = document.querySelector('.form__terms-label-error__text');
    errorCheckbox.classList.remove('form__terms-label-error');
  };

  this.send = async () => {
    const regButton = document.getElementById('reg-button');
    const agreeCheckbox = document.getElementById('checkbox');
    const emailInput = document.getElementById('email');
    if (regButton.disabled) return;
    // отключаем кнопку
    regButton.disabled = true;

    const r = await register({
      email: emailInput.value,
      agreed: agreeCheckbox.checked,
      source: 'cyberpunk-landing',
    });

    that.clearErrors();
    this.clearErrors();

    regButton.disabled = false;

    if (r.result === 'error') {
      const errorCheckbox = document.querySelector('.form__terms-label-error__text');
      const { errors } = r;
      if (errors.agreed) {
        document.getElementsByClassName('form__terms-label-error__text')[0].innerHTML = errors.agreed.join('; ');
        errorCheckbox.classList.add('form__terms-label-error');
      }
      if (errors.email) {
        const mailField = document.querySelector('input[type=email]');
        const emailFieldParent = document.querySelector('.form__input-wrapper');
        emailFieldParent.classList.add('form__input-error');
        document.querySelector('.form__input-error__text').classList.add('form__terms-label-error');
        document.getElementsByClassName('form__input-error__text')[0].innerHTML = errors.email.join('; ');
        mailField.style.border = '2px solid #ff0000';
      }
    } else {
      modal.classList.add('open');
      document.querySelector('.modal-email').innerHTML = emailInput.value;
      getRegisterHref();
    }
  };
}
window.sendForm = new SendForm();

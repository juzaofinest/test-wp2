export const getSupportedLanguages = () => {
  const req = require.context('./translations', true, /^(?!.*__tests__)(?:.*)?\.json$/);
  return req.keys().map(k => k.replace('./', '').replace('.json', ''));
};

getSupportedLanguages();

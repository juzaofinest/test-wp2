/* eslint-disable func-names */
import Handlebars from 'handlebars/dist/handlebars';
import _ from 'lodash';
import 'url-polyfill';
import queryString from 'query-string';

import 'reset.css';
import 'normalize.css';
import './style.css';
import '../Fonts/PFDinDisplayPro/stylesheet.css';

import someVideo from './footage.mp4';
import clickInputs from './clickInput';
import editEmail from './editEmail';
import SendForm from './Sendform';
import { getUserLang } from './../api';

window.__yourbet_redirect_path = '/games/all'; // eslint-disable-line no-underscore-dangle

const videoload = () => {
  const s = document.createElement('source');
  s.src = someVideo;
  s.type = 'video/mp4';
  const video = document.querySelector('.video');
  if (video != null) {
    video.classList.add('video');
    video.setAttribute('autoplay', '');
    video.setAttribute('muted', '');
    video.setAttribute('loop', '');
    video.appendChild(s);
  }
};

export default function updateParams() {
  const url = new URL(window.location.href);
  const redirectButton = document.getElementById('redirect-href');
  // const lang = [ 'ru', 'en', 'tr', 'uk', 'ka' ].includes(translate('language')) ? translate('language') : 'en';
  const lang = [ 'en', 'ru', 'tr' ];
  let existLang = url.searchParams.get('lang');
  const returnLang = () => {
    if (lang.includes(existLang)) {
      return existLang;
    }
    return existLang = '';
  };
  const params = [ 'cid', 'postback', 'utm_term' ];
  returnLang();
  const searchPrams = queryString.parse(window.location.search);
  let paramsString = params.map((p) => {
    const val = searchPrams[p];
    return val ? `${p}=${val}` : '';
  }).filter(Boolean).join('&');
  if (paramsString.length) {
    paramsString = `?${paramsString}`;
  }
  const returnUrl = `https://yourbet.com/${existLang}${paramsString}`;
  redirectButton.href = returnUrl;
  return returnUrl;
}
(async function () {
  const lang = getUserLang([
    'en', 'ar', 'da', 'de', 'fi', 'hr', 'no', 'pl', 'ru', 'sv', 'tr', 'uk', 'cz', 'bg', 'az', 'el', 'ge', 'hu', 'is', 'kg', 'ro', 'sr', 'tm',
  ]);
  const translations = await import(`./translations/${lang}.json`);
  window.data = {
    header: _.get(translations, 'Header.descr', 'Make a deposit and get bonuses'),
    bonus1: _.get(translations, 'MainContent.BonusOne', 'Bonus +200%'),
    bonusDescr1: _.get(translations, 'MainContent.BonusDescr', 'Up to 1000 $/€'),
    bonus2: _.get(translations, 'MainContent.BonusTwo', 'Free spins'),
    popupHeader: _.get(translations, 'PopupField.popupHeader', 'Check your email'),
    bonusDescr2: _.get(translations, 'MainContent.BonusDescr2', 'Up to 100 free spins'),
    placeholder: _.get(translations, 'EmailField.emailPlaceholder', 'Your email'),
    edit: _.get(translations, 'EmailField.emailEdit', 'Edit'),
    bonusButton: _.get(translations, 'EmailField.bonusButton', 'Get 200% bonus'),
    wait: 'Loading',
    modalWindowText: _.get(translations, 'PopupField.popupField', 'Please click on the activation link sent to you by email'),
    termsIagree: _.get(translations, 'TandC.iAgreeWith', 'I agree with'),
    allrightBtn: _.get(translations, 'PopupField.popupProof', 'OK'),
    termsHref: _.get(translations, 'TandC.termsAndConditions', 'terms and conditions'),
    termsAnd: _.get(translations, 'TandC.and', 'and'),
    termsPrivacyPolicy: _.get(translations, 'TandC.privacyPolicy', 'privacy policy'),
    termsCitizen: _.get(translations, 'TandC.notACitizen', 'I am not a citizen of United States of America, Spain, Italy, France, United Kingdom, Russian Federation, Israel, Latvia'),
  };
  let v = queryString.parse(window.location.search).v || '1';
  if (![ '1', '5' ].includes(v)) v = '1';
  const template1 = await import(`./templates/template${v}.html` /* webpackChunkName: `chunk-${chunkName}` */);

  const compiledTemplate = Handlebars.compile(template1.default);
  const complete = compiledTemplate(window.data);
  document.getElementById('target').innerHTML = complete;

  updateParams();
  window.sendForm = new SendForm();
  editEmail();
  const emailInput = document.getElementById('email');
  // console.log(clickInputs);
  emailInput.addEventListener('input', clickInputs);
  videoload();
}());


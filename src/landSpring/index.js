import Handlebars from 'handlebars/dist/handlebars';
import 'reset.css';
import 'normalize.css';
import '../Fonts/PFDinDisplayPro/stylesheet.css';
// import './../AvenirNext/stylesheet.css';
import '../Fonts/AvenirNext/stylesheet.css';
import './style.styl';
import './modal.css';
import _ from 'lodash';
import queryString from 'query-string';
import './reg-link.css';
import { getTranslations, register } from '../api';


const backendUrl = process.env.LANDINGS_API_URL;
const transUrl = `${backendUrl}/translations/spring${window.location.search}`;

const getRegisterHref = () => {
  const redirectButton = document.getElementById('redirect-href');
  // const lang = [ 'ru', 'en', 'tr', 'uk', 'ka' ].includes(translate('language')) ? translate('language') : 'en';
  const lang = 'en';
  const params = [ 'cid', 'postback', 'utm_term' ];
  const searchPrams = queryString.parse(location.search);
  let paramsString = params.map((p) => {
    const val = searchPrams[p];
    return val ? `${p}=${val}` : '';
  }).filter(Boolean).join('&');
  if (paramsString.length) {
    paramsString = `?${paramsString}`;
  }
  const returnUrl = `https://yourbet.com/${lang}/register${paramsString}`;
  redirectButton.href = returnUrl;
  return returnUrl;
};

fetch(transUrl, {
  credentials: 'include',
})
  .then(response => response.json())
  .catch(() => {
  })
  .then((translations) => {
    const data = {
      header: _.get(translations, 'Header.descr', 'Global Warming'),
      headerDescr: _.get(translations, 'Header.descr2', 'Days are longer - bonuses are warmer!'),
      content: _.get(translations, 'MainContent.mainContentDescr', 'Welcome bonus of up to'),
      contentDescr: _.get(translations, 'MainContent.mainContentDescr2', 'free spins!'),
      placeholder: _.get(translations, 'EmailField.emailPlaceholder', 'Your email'),
      email: _.get(translations, 'EmailField.emailField', 'Take part'),
      modalWindowText: _.get(translations, 'PopupField.popupField', 'Please click on the activation link sent to you by email'),
      termsIagree: _.get(translations, 'TandC.iAgreeWith', 'I agree with'),
      allrightBtn: _.get(translations, 'PopupField.popupProof', 'OK'),
      termsHref: _.get(translations, 'TandC.termsAndConditions', 'terms and conditions'),
      termsAnd: _.get(translations, 'TandC.and', 'and'),
      termsPrivacyPolicy: _.get(translations, 'TandC.privacyPolicy', 'privacy policy'),
      termsCitizen: _.get(translations, 'TandC.notACitizen', 'I am not a citizen of United States of America, Spain, Italy, France, United Kingdom, Russian Federation, Israel, Latvia'),
      footerSports: _.get(translations, 'Footer.footerSport', 'Sports'),
      footerSports2: _.get(translations, 'Footer.footerSport2', ''),
      footerGames: _.get(translations, 'Footer.footerGames', 'games'),
      footerSupport: _.get(translations, 'Footer.footerSupport', 'Support service'),

    };
    const template = document.getElementById('template').innerHTML;
    const compiled_template = Handlebars.compile(template);
    const complete = compiled_template(data);
    document.getElementById('target').innerHTML = complete;
    const regButton = document.getElementById('reg-button');
    // getRegisterHref();
  });
function SendForm() {
  const that = this;
  const email = '';

  this.clearErrors = function () {
    document.querySelector('.form__input-wrapper').classList.remove('form__input-error');
    document.getElementsByClassName('form__terms-label-error__text')[0].innerHTML = '';
    document.getElementsByClassName('form__input-error__text')[0].innerHTML = '';
    const errorCheckbox = document.querySelector('.form__terms-label-error__text');
    errorCheckbox.classList.remove('form__terms-label-error');
  };

  this.send = async () => {
    const regButton = document.getElementById('reg-button');
    const agreeCheckbox = document.getElementById('checkbox');
    const emailInput = document.getElementById('email');
    if (regButton.disabled) return;
    // отключаем кнопку
    regButton.disabled = true;

    const r = await register({
      email: emailInput.value,
      agreed: agreeCheckbox.checked,
      source: 'spring-landing',
    });

    that.clearErrors();
    this.clearErrors();

    regButton.disabled = false;

    if (r.result === 'error') {
      const errorCheckbox = document.querySelector('.form__terms-label-error__text');
      const { errors } = r;
      if (errors.agreed) {
        document.getElementsByClassName('form__terms-label-error__text')[0].innerHTML = errors.agreed.join('; ');
        errorCheckbox.classList.add('form__terms-label-error');
      }
      if (errors.email) {
        const mailField = document.querySelector('input[type=email]');
        const emailFieldParent = document.querySelector('.form__input-wrapper');
        emailFieldParent.classList.add('form__input-error');
        document.querySelector('.form__input-error__text').classList.add('form__terms-label-error');
        document.getElementsByClassName('form__input-error__text')[0].innerHTML = errors.email.join('; ');
        mailField.style.border = '2px solid #ff0000';
      }
    } else {
      modal.classList.add('open');
      document.querySelector('.modal-email').innerHTML = emailInput.value;
      getRegisterHref();
      // modal.querySelector('.modal-email span').innerHTML = email;
    }
  };
}
window.sendForm = new SendForm();

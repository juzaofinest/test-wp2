/* eslint-disable func-names */
import Handlebars from 'handlebars/dist/handlebars';
// import 'picturefill';
import _ from 'lodash';
import 'url-polyfill';
import queryString from 'query-string';

import 'reset.css';
import 'normalize.css';
import './style.css';
import '../Fonts/PFDinDisplayPro/stylesheet.css';

import someVideo from './footage.mp4';
import clickInputs from './clickInput';
import editEmail from './editEmail';
import SendForm from './Sendform';
import { getUserLang } from './../api';
// import returnUrl from './variables';

window.__yourbet_redirect_path = '/betting'; // eslint-disable-line no-underscore-dangle

const videoload = () => {
  const s = document.createElement('source');
  s.src = someVideo;
  s.type = 'video/mp4';
  const video = document.querySelector('.video');
  if (video != null) {
    video.classList.add('video');
    video.setAttribute('autoplay', '');
    video.setAttribute('muted', '');
    video.setAttribute('loop', '');
    video.appendChild(s);
  }
};

export default function updateParams() {
  const url = new URL(window.location.href);
  const redirectButton = document.getElementById('redirect-href');
  // const lang = [ 'ru', 'en', 'tr', 'uk', 'ka' ].includes(translate('language')) ? translate('language') : 'en';
  const lang = [ 'en', 'ru', 'tr' ];
  let existLang = url.searchParams.get('lang');
  const returnLang = () => {
    if (lang.includes(existLang)) {
      return existLang;
    }
    existLang = 'en';
    return existLang;
  };
  const params = [ 'cid', 'postback', 'utm_term' ];
  returnLang();
  const searchPrams = queryString.parse(window.location.search);
  let paramsString = params.map((p) => {
    const val = searchPrams[p];
    return val ? `${p}=${val}` : '';
  }).filter(Boolean).join('&');
  if (paramsString.length) {
    paramsString = `?${paramsString}`;
  }
  const returnUrl = `https://yourbet.com/${existLang}betting${paramsString}`;
  // redirectButton.href = returnUrl;
  return returnUrl;
}
(async function () {
  const lang = getUserLang([
      'en', 'ar', 'da', 'de', 'fi', 'hr', 'no', 'pl', 'ru', 'sv', 'tr', 'uk', 'cz', 'bg', 'az', 'el', 'ge', 'hu', 'is', 'kg', 'ro', 'sr', 'tm', 'am',
  ]);
  const translations = await import(`./translations/${lang}.json`);

  window.data = {
    header: _.get(translations, 'Header.descr', 'Double your first deposit'),
    bonus1: _.get(translations, 'MainContent.BonusOne', 'Up to $/€ 555'),
    bonusDescr1: _.get(translations, 'MainContent.BonusDescr', 'for betting'),
    popupHeader: _.get(translations, 'PopupField.popupHeader', 'Check your email'),
    bonusDescr2: _.get(translations, 'MainContent.BonusDescr', 'Up to 100 free spins'),
    wait: 'Loading',
    placeholder: _.get(translations, 'EmailField.emailPlaceholder', 'Your email'),
    edit: _.get(translations, 'EmailField.emailEdit', 'Edit'),
    bonusButton: _.get(translations, 'EmailField.bonusButton', 'Get bonus'),
    modalWindowText: _.get(translations, 'PopupField.popupField', 'Please click on the activation link sent to you by email'),
    termsIagree: _.get(translations, 'TandC.iAgreeWith', 'I agree with'),
    allrightBtn: _.get(translations, 'PopupField.popupProof', 'OK'),
    termsHref: _.get(translations, 'TandC.termsAndConditions', 'terms and conditions'),
    termsAnd: _.get(translations, 'TandC.and', 'and'),
    termsPrivacyPolicy: _.get(translations, 'TandC.privacyPolicy', 'privacy policy'),
    termsCitizen: _.get(translations, 'TandC.notACitizen', 'I am not a citizen of United States of America, Spain, Italy, France, United Kingdom, Russian Federation, Israel, Latvia'),
  };
  const template1 = await import('./templates/template1.html' /* webpackChunkName: `chunk-${chunkName}` */);

  const compiledTemplate = Handlebars.compile(template1.default);
  // var regButton = document.getElementById('reg-button');
  const complete = compiledTemplate(window.data);
  document.getElementById('target').innerHTML = complete;
  // const regButton = document.getElementById('reg-button');
  updateParams();
  window.sendForm = new SendForm();
  editEmail();
  const emailInput = document.getElementById('email');
  emailInput.addEventListener('input', clickInputs);
  videoload();
}());


import { register } from '../api';
import updateParams from './index.js';

export default function SendForm() {
  const that = this;

  this.clearErrors = () => {
    // const agreeCheckbox = document.getElementById('agree');
    document.querySelector('.form__input-wrapper').classList.remove('form__input-error');
    document.getElementsByClassName('form__terms-label-error__text')[0].innerHTML = '';
    document.getElementsByClassName('form__input-error__text')[0].innerHTML = '';
    const errorCheckbox = document.querySelector('.form__terms-label-error__text');
    errorCheckbox.classList.remove('form__terms-label-error');
  };

  this.send = async () => {
    const errorCheckbox = document.querySelector('.form__terms-label-error__text');
    const preloader = document.querySelector('.preloader');
    const regButton = document.getElementById('reg-button');
    const labelError = document.querySelector('.form__terms-label');
    labelError.classList.remove('form__terms-label-error');
    errorCheckbox.classList.remove('visible');
    errorCheckbox.classList.add('hidden');
    const agreeCheckbox = document.getElementById('agree');
    const emailInput = document.getElementById('email');
    if (regButton.disabled) return;
    // отключаем кнопку
    regButton.disabled = true;
    preloader.classList.remove('hidden');
    document.getElementById('reg-button').innerHTML = data.wait;


    const r = await register({
      email: emailInput.value,
      agreed: agreeCheckbox.checked,
      source: 'yourbet-landing',
    });
    that.clearErrors();

    regButton.disabled = false;
    preloader.classList.add('hidden');
    document.getElementById('reg-button').innerHTML = data.bonusButton;
    console.log(data.wait);
    if (r.result === 'error') {
      const { errors } = r;
      if (errors.agreed) {
        labelError.classList.add('form__terms-label-error');
        document.getElementsByClassName('form__terms-label-error__text')[0].innerHTML = errors.agreed.join('; ');
        errorCheckbox.classList.add('visible');
      }
      if (errors.email) {
        const emailFieldParent = document.querySelector('.form__input-wrapper');
        emailFieldParent.classList.add('form__input-error');
        document.querySelector('.form__input-error__text').classList.add('form__terms-label-error');
        document.getElementsByClassName('form__input-error__text')[0].innerHTML = errors.email.join('; ');
      }
    } else {
      // modal.classList.add('open');
      errorCheckbox.classList.remove('visible');
      document.querySelector('.modal-email').innerHTML = emailInput.value;
    }
  };
}

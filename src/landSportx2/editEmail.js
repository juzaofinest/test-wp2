export default function editEmail() {
    function edit() {
        var regButton = document.getElementById('reg-button');
        var form = document.querySelector('form');
        var modal = document.getElementById('modal');
        form.reset();
        regButton.disabled = false;
        const emailInput = document.getElementById('email');
        document.querySelector('.modal-email').innerHTML = emailInput.value;
        emailInput.value = '';
        modal.classList.remove('open');
    }
        var editButton = document.querySelector('.edit-button');
        editButton.addEventListener('click', edit);
    };

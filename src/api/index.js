import * as mocks from './mock';
import * as registerApi from './register';
import * as translateApi from './translate';

const prod = process.env.NODE_ENV === 'production';

export const register = prod ? registerApi.register : mocks.register;
export const getTranslations = prod ? translateApi.translate : mocks.getTranslations;
export const { checkAvailability, getUserLang } = registerApi;


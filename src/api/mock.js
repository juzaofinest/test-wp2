// @flow
/* eslint-disable no-alert */
import _ from 'lodash';
import queryString from 'query-string';

const yourbetUrl = process.env.YOURBET_URL || 'http://proxy-test.yourbet.com';

export const register = async (values) => {
  const parsed = queryString.parse(window.location.search);
  const {
    cid,
    postback,
  } = parsed;

  let { lang } = parsed;
  if (![ 'en', 'ru', 'es', 'pl', 'tr', 'ka', 'uk', 'pt' ].includes(lang)) lang = 'en';

  const errors = {};

  if (values.email === 'r@ya.ru') {
    errors.email = ['e-mail exists'];
  }

  if (values.agreed !== true) {
    errors.agreed = ['Вам надо принять лицензионное соглашение чтобы продолжить'];
  }

  if (!_.isEmpty(errors)) {
    return {
      result: 'error',
      errors,
    };
  }
  await new Promise(resolve => setTimeout(resolve, 700));

  setTimeout(() => {
    // eslint-disable-next-line no-underscore-dangle
    alert(`here must be redirect to ${yourbetUrl}/${lang}${window.__yourbet_redirect_path || ''}?${queryString.stringify({ cid, postback })}`);
  }, 200);

  return {
    result: 'ok',
  };
};

export const getTranslations = async (url: string) => ({
  lang: 'def',
  url,
});

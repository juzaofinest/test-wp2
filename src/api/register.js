// @flow
// import _ from 'lodash';
import acceptLanguage from 'accept-language';
import queryString from 'query-string';
import browserLocale from 'browser-locale';
import PromiseWindow from 'promise-window';
import 'whatwg-fetch';

const yourbetUrl = process.env.YOURBET_URL || 'http://proxy-test.yourbet.com';
// alert(yourbetUrl);

export type RegisterFormValues = {|
  email: string,
  agreed: boolean,
  source: string,
|};

export type RegisterResponseType =
  | {| result: 'ok' |}
  | {
    result: 'error',
    errors: {|
      email?: Array<string>,
      agreed?: Array<string>,
    |}
  };

const fetchEmailExists = async ({ email }: {
  email: string,
}) => {
  const emailExistsRes = await fetch(`${yourbetUrl}/api/v1/profiles/email`, {
    credentials: 'include',
    body: JSON.stringify({ email }),
    method: 'PUT',
    headers: {
      Accept: 'application/json, text/plain, */*',
      // 'Content-Type': 'application/json',
    },
  });
  const { result } = await emailExistsRes.json();
  console.log('emailExists? =>', result);
  return result === false;
};

const fetchRegisterRes = async (enhancedValues: Object) => {
  const registerRes = await fetch(`${yourbetUrl}/api/v1/profiles`, {
    credentials: 'include',
    body: JSON.stringify({
      currency: 'EUR',
      ...enhancedValues,
    }),
    method: 'POST',
    headers: {
      Accept: 'application/json, text/plain, */*',
      // 'Content-Type': 'application/json',
    },
  }).then(r => r.json());
  return registerRes;
};

export const register = async (values: RegisterFormValues): Promise<RegisterResponseType> => {
  const enhancedValues = { ...values };
  const parsed = queryString.parse(window.location.search);
  const {
    cid,
    postback,
  } = parsed;

  let { lang } = parsed;

  /** workaround to set yourbet.com third party cookies */
  await PromiseWindow.open(`${yourbetUrl}?setcookie`).then(console.log).catch(console.log);

  if (![ 'en', 'ru', 'es', 'pl', 'tr', 'ka', 'uk', 'pt' ].includes(lang)) lang = 'en';

  if (cid && postback) {
    enhancedValues.extProfile = {
      customParams: queryString.stringify({ cid, postback }),
    };
  }

  try {
    if (await fetchEmailExists(enhancedValues)) {
      return {
        result: 'error',
        errors: {
          email: ['e-mail exists'],
        },
      };
    }

    const registerRes = await fetchRegisterRes(enhancedValues);

    if (!registerRes.result) {
      return {
        result: 'error',
        errors: {
          email: ['register error'],
        },
      };
    }
  } catch (error) {
    window.location.href = `${yourbetUrl}/${lang}/?${queryString.stringify({ cid, postback })}`;
    return {
      result: 'error',
      errors: {
        email: ['site is not available in your region'],
      },
    };
  }
  (window.dataLayer || []).push({ event:'form_submit' });
  // eslint-disable-next-line no-underscore-dangle
  window.location.href = `${yourbetUrl}/${lang}${window.__yourbet_redirect_path || ''}?${queryString.stringify({ cid, postback })}`;

  return {
    result: 'ok',
  };
};

export default { register };

export const checkAvailability = async () => {
  try {
    await fetch(`${yourbetUrl}/api/v1/profiles/email`, {
      // credentials: 'same-origin',
      method: 'OPTIONS',
      headers: {
        Accept: 'application/json, text/plain, */*',
      },
    }).then(r => r.json());
    return true;
  } catch (e) {
    return false;
  }
};

export const getUserLang = (available: Array<string> = ['en']) => {
  acceptLanguage.languages(available);
  const parsed = queryString.parse(window.location.search);
  let { lang } = parsed;
  if (!available.includes(lang)) {
    lang = acceptLanguage.get(browserLocale());
  }
  if (!available.includes(lang)) {
    [lang] = available;
  }
  return lang;
};

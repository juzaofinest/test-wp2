export const translate = url => fetch(`${process.env.LANDINGS_API_URL || ''}${url}`, {
  credentials: 'include',
  method: 'get',
  headers: {
    Accept: 'application/json, text/plain, */*',
    'Content-Type': 'application/json',
  },
}).then(r => r.json());

